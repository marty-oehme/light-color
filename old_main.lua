--all the available libs listed
local HC = require "lib.HC"
inspect = require 'lib.inspect'
vector = require 'lib.hump.vector'
lume = require 'lib.lume'
ProFi = require 'lib.ProFi'


local intersect = {}
local triangulatedintersect = {}

local collider = {}

local obstacles = {}

local lightrect, lightrect2, light
local lightCanvas, lightCanvas2

local ShadowCaster = {}

function love.load(arg)
    love.window.setMode( 1280, 720, { vsync = true} )
    love.graphics.setDefaultFilter( 'nearest', 'nearest' )

--  *************************

    lightcolor = LightColor
    collider = HC.new(200)

    scale = 2
    obstaclenr = 25

    -- create a buncha colliders = obstacles
    for i=1, obstaclenr do
        local count = 1
        while count > 0 do
            obstacles[i] = collider:rectangle(love.math.random(0, 1200), love.math.random(0, 700), love.math.random(40, 200), love.math.random(40, 200))
            count = 0
            for _,_ in pairs(collider:collisions(obstacles[i])) do
                count = count + 1
            end
            if count == 0 then break end
            collider:remove(obstacles[i])
        end
    end

    -- create our dynamic light source
    light = love.graphics.newImage("light_softer.png")
    lightrect = collider:rectangle(-1000, -1000, light:getWidth()*scale, light:getHeight()*scale)
    lightrect.image = light
    lightrect.light = true

    -- create a second dynamic light
    lightrect2 = collider:rectangle(100, 100, light:getWidth()*scale, light:getHeight()*scale)
    lightrect2.image = light
    lightrect2.light = true

    -- create the canvas we will use to blend the light and shadow render
    -- lightCanvas = love.graphics.newCanvas()
    -- lightCanvas2 = love.graphics.newCanvas()

    -- create shadowmaps
    shadowcast1 = ShadowCaster:new()
    shadowcast2 = ShadowCaster:new()

    manylights = {}
    manycasters = {}
    -- for i=1, 15 do
    --     manycasters[i] = ShadowCaster:new()
    --     manylights[i] = collider:rectangle(love.math.random(0, 500), love.math.random(0, 700), light:getWidth()*scale, light:getHeight()*scale)
    --     manylights[i].image = light
    --     manylights[i].light = true
    --     manylights[i].color = {love.math.random(0, 155), love.math.random(0, 155), love.math.random(0, 155)}
    -- end
    -- local i = 0
    -- for y=1, 3 do
    --     for x = 1, 3 do
    --         i = i+1
    --         manycasters[i] = ShadowCaster:new()
    --         manylights[i] = collider:rectangle(100+x*2, 100+y*2, light:getWidth()*scale, light:getHeight()*scale)
    --         manylights[i].image = light
    --         manylights[i].light = true
    --         manylights[i].color = {love.math.random(50, 70), love.math.random(5, 10), love.math.random(0, 0)}
    --     end
    -- end

end



function love.update(dt)

--  *************************
    lightrect:moveTo(love.mouse.getPosition())

    lightrect2:move(25*dt, 0)

    for i=1, #manylights do
        manylights[i]:move(love.math.random(50, 50)*dt, 0)
        local cenX, cenY = manylights[i]:center()
        if cenX > 1300 then
            manylights[i]:moveTo(1, cenY)
        end
    end

end


-- ********* LIGHT ALGORITHM START

function ShadowCaster:new()
    self.lightCanvas = love.graphics.newCanvas()

    return lume.clone(self)
end

function ShadowCaster:calculateShadows( collisionsystem, emitterShape)
    local light = emitterShape

    -- get all shapes the lightbox collides with
    self.shadowcolliders = self:gatherCollisions(light, collisionsystem)


    -- gather all vectors (edges) of the shapes colliding
    -- with the bounding box of light emitter
    -- keep only those pointing 'towards' the light (possibly affected by it)
    self.closestEdges = self:findCloseEdges(light, self.shadowcolliders)

    -- if closest edges returns false the light is inside some obstacle, do not draw it
    if self.closestEdges == false then return false end

    -- check if the edge itself is within the lightbox area
    -- if not, discard it
    -- (mostly useful for polygonal shapes, in which edges might jut out)
    for i = 1, #self.closestEdges do
        if self.closestEdges[i] and not self:edgeCollisionCheck(light, self.closestEdges[i].a, self.closestEdges[i].b) then
            table.remove( self.closestEdges, i )
            i = i-1
        end
    end

    -- clip edge to lightbox borders (Lian Barsky algo?)
    -- nothing remaining is outside emitter box
    -- only works for rectangle bbox afaik?
    for i = 1, #self.closestEdges do
        local _, clipUp = light:support(0,1)
        local _, clipDown = light:support(0,-1)
        local newA, newB = self:_clipEdge(light:support(-1, 0), clipDown, light:support(1,0), clipUp, self.closestEdges[i].a, self.closestEdges[i].b)
        if newA ~= false then
            self.closestEdges[i].a = newA
            self.closestEdges[i].b = newB
        end
    end

    -- work out the vectors to all of the active edge start/end vertices
    -- add them to a list with reference to their corresponding edge
    -- and their angle (viewed from emitter)
    self.activeRays = self:castRays(light, self.closestEdges)


    -- finally add lightbox edges and rays to corresponding lists
    self:addEmitterBounds(light, self.closestEdges, self.activeRays)

    -- first calculate the angles of the rays to the emitter, then
    -- sort the rays table according to their angle
    self:sortRaysByAngle(light, self.activeRays)

    -- iterate through active Rays and create a triangle fan
    self.intersections = self:createIntersectionFan(light, self.closestEdges, self.activeRays)

    -- triangulate the fan, so it can be drawn with the polygon function

    self.triangles = self:triangulateIntersections(light, self.intersections)

    return self.triangles
end

function ShadowCaster:gatherCollisions(emitterShape, collisionsystem)
    local colsOut = collisionsystem:collisions(emitterShape)

    -- gathers all collisions, excepting those of other lights in the same system
    for coll, _ in pairs(colsOut) do
        if coll.light then
            colsOut[coll] = nil
        end
    end
    return colsOut
end

function ShadowCaster:findCloseEdges(lightbox, colliders)
    local closestEdges = {}
    for k, v in pairs(colliders) do
        local verts = k._polygon.vertices
        local allinside = true
        for i = 1, #verts do
            local startvert = verts[i]
            local endvert = verts[i+1] or verts[1]

            local lightX, lightY = lightbox:center()
            local emittervec = vector(startvert.x - lightX, startvert.y - lightY)
            local edgevec = vector(endvert.x - startvert.x, endvert.y - startvert.y)

            -- find the closest vectors
            -- by multiplying the normal of edgevector with emitter -> start/end vertice vector (dot product)
            -- if normal is positive > 'outside' vector (= close and shined on by light)
            -- if normal is negative > 'inside' vector (not hit by light)
            local edgenormal = edgevec:perpendicular():normalizeInplace()
            if edgenormal * emittervec > 0 then
                closestEdges[#closestEdges+1] = { vec = edgevec, a = startvert, b = endvert }
                allinside = false
            end
        end

        -- if all edges found are inside edges that means light is currently
        -- 'inside' an obstacle -> break the whole process (returns false)
        if allinside == true then return false end
    end
    return closestEdges
end

function ShadowCaster:edgeCollisionCheck(light, edgestartvert, edgestopvert)
    local min = math.min
    local max = math.max
    if light:contains(min(edgestartvert.x, edgestopvert.x), min(edgestartvert.y, edgestopvert.y)) or
        light:contains(min(edgestartvert.x, edgestopvert.x), max(edgestartvert.y, edgestopvert.y)) or
        light:contains(max(edgestartvert.x, edgestopvert.x), min(edgestartvert.y, edgestopvert.y)) or
        light:contains(max(edgestartvert.x, edgestopvert.x), max(edgestartvert.y, edgestopvert.y)) then
            return true
    end
    return false
end

function ShadowCaster:_clipEdge(clipLeft, clipDown, clipRight, clipUp, edgestartvert, edgestopvert)
    -- preliminary setup
    local t0, t1 = 0.0, 1.0
    local P0, P1 = edgestartvert, edgestopvert
    local dx, dy = P1.x - P0.x, P1.y - P0.y

    -- Liang-Basrky line clipping algorithm testing the edges
    local clipAlgo = function(P, Q, R)
        --edge check itself
        -- if all the line is out of box (i.e. parrallel to  edge) we can stop here
        if P == 0 and Q < 0 then
            return false
        -- observe the edge (in case of line going away from edge)
        elseif P < 0 then
            if R > t1 then
                return false
            -- line is actually longer than edge -> clip it!
            elseif R > t0 then
                t0 = R
            end
        -- case: line coming towards edge
        elseif P > 0 then
            if R < t0 then
                return false
            elseif R < t1 then
                t1 = R
            end
        end
    end

    --left edge check
    local P = -dx
    local Q = -1*(clipLeft-P0.x)
    local R = Q/P
    clipAlgo(P, Q, R)
    --right edge check
    local P = dx
    local Q = clipRight-P0.x
    local R = Q/P
    clipAlgo(P, Q, R)
    --down edge check
    local P = -dy
    local Q = -(clipDown-P0.y)
    local R = Q/P
    clipAlgo(P, Q, R)
    --up edge check
    local P = dy
    local Q = clipUp-P0.y
    local R = Q/P
    clipAlgo(P, Q, R)

    -- finally calculate new start and end point, based on t0, t1
    if t0 == 0 and t1 == 1 then
        -- nothing to do, line was already completely inside
        return P0, P1
    else
        -- return new start and end point
        local newP0 = { x = P0.x + (t0*dx), y = P0.y + t0*dy }
        local newP1 = { x = P0.x + t1*dx, y = P0.y + t1*dy }

        return newP0, newP1
    end
end

function ShadowCaster:castRays(emitterShape, edgeList)

    local activeRays = {}
    for i = 1, #edgeList do
        local P = {}
        local emvert = {}
        local emitterX, emitterY = emitterShape:center()

        P.x = edgeList[i].a.x-emitterX
        P.y = edgeList[i].a.y-emitterY
        P = vector(P.x, P.y)
        activeRays[#activeRays+1] = { vec = vector(P.x, P.y), edgeid = i, a = { x = emitterX, y = emitterY }, b = { x = edgeList[i].a.x, y = edgeList[i].a.y}  }

        P = vector(P.x, P.y)
        P.x = edgeList[i].b.x-emitterX
        P.y = edgeList[i].b.y-emitterY
        activeRays[#activeRays+1] = { vec = vector(P.x, P.y), edgeid = i, a = { x = emitterX, y = emitterY }, b = { x = edgeList[i].b.x, y = edgeList[i].b.y} }
    end

    return activeRays

end

function ShadowCaster:addEmitterBounds(emitterShape, edgeList, rayList)
    local verts = emitterShape._polygon.vertices
    for i = 1, #verts do

        -- finally add edges of lightbox to active edges
        local startvert = verts[i]
        local endvert = verts[i+1] or verts[1]
        local edgevec = vector(endvert.x - startvert.x, endvert.y - startvert.y)

        edgeList[#edgeList+1] = { vec = edgevec, a = startvert, b = endvert }


        -- and rays to lightbox corners to active rays
        local emitterX, emitterY = emitterShape:center()
        local cornerRayStart = vector(startvert.x - emitterX, startvert.y - emitterY)
        local cornerRayEnd = vector(endvert.x - emitterX, endvert.y - emitterY)

        rayList[#rayList+1] = { vec = cornerRayStart, edgeid = #edgeList, a = { x = emitterX, y = emitterY }, b = startvert }
        rayList[#rayList+1] = { vec = cornerRayEnd, edgeid = #edgeList, a = { x = emitterX, y = emitterY }, b = endvert }

    end
end

function ShadowCaster:sortRaysByAngle(emitterShape, rayList)
    local emitterX, emitterY = emitterShape:center()
    table.sort(rayList, function(a, b) return lume.angle(a.a.x, a.a.y, a.b.x, a.b.y) < lume.angle(b.a.x, b.a.y, b.b.x, b.b.y) end)
end

function ShadowCaster:createIntersectionFan(emitterShape, edgeList, rayList)
    local centerX, centerY = emitterShape:center()
    local intersect = {}

    for indR, R in ipairs(rayList) do
        local dist = math.huge
        local P_self = {}
        local P_other = {}

        for indE, E in ipairs(edgeList) do
            local resultX, resultY = self:_findIntersection(R.a, R.b, E.a, E.b)

            -- found intersection
            if resultX ~= false then

                -- round to pixel value, makes it easier and more reliable
                -- to work with & compare to other results
                local result = vector(math.floor(resultX), math.floor(resultY))
                local fromEmitter = vector(resultX - R.a.x, resultY - R.a.y)

                -- if this ray belongs to the edge that it intersects with
                -- add it as P_self (so we can later see if the ray hits something closer)
                if R.edgeid == indE then
                    P_self = result
                    if result.x == E.a.x and result.y == E.a.y then
                        -- ray belongs to left edgevert
                        P_self.edgevert = 2
                    elseif result.x == E.b.x and result.y == E.b.y then
                        -- ray belongs to right edgevert
                        P_self.edgevert = 1
                    end
                elseif fromEmitter:len2() < dist then

                    P_other = result
                    dist = fromEmitter:len2()
                end
            end

        end

        local P_selfVec = vector((P_self.x or math.huge)-R.a.x, (P_self.y or math.huge)-R.a.y)
        local P_otherVec = vector((P_other.x or math.huge)-R.a.x, (P_other.y or math.huge)- R.a.y)

        -- the vert is towards the right end of the edge -> add it before the other vert
        if P_self.edgevert == 2 and P_selfVec:len2() < P_otherVec:len2() then
            intersect[#intersect+1] = P_self
        end

        if P_other.x ~= nil and P_other ~= intersect[#intersect] then
            intersect[#intersect+1] = P_other
        end

        -- the vert is towards the left end of the edge -> add it before the other vert
        if P_self.edgevert == 1 and P_selfVec:len2() < P_otherVec:len2() then
            intersect[#intersect+1] = P_self
        end

    end

    return intersect
end

function ShadowCaster:triangulateIntersections(emitterShape, intersectionList)
    local vertpositions = {}
    for i = 1, #intersectionList do
        local centerX, centerY = emitterShape:center()
        local nextpos = intersectionList[i+1] or intersectionList[1]
        vertpositions[#vertpositions+1] = { centerX, centerY, intersectionList[i].x, intersectionList[i].y, nextpos.x, nextpos.y}
    end
    return vertpositions
end

-- http://stackoverflow.com/a/1968345
function ShadowCaster:_findIntersection(p1, p2, p3, p4)
    local s1x = p2.x - p1.x
    local s1y = p2.y - p1.y
    local s2x = p4.x - p3.x
    local s2y = p4.y - p3.y

    local s = 0.0
    local t = 0.0
    s = (-s1y * (p1.x - p3.x) + s1x * (p1.y - p3.y)) / (-s2x * s1y + s1x * s2y)
    t = ( s2x * (p1.y - p3.y) - s2y * (p1.x - p3.x)) / (-s2x * s1y + s1x * s2y)

    if (s >= 0 and s <=1 and t >= 0 ) then
        --collision detected
        return (p1.x+(t*s1x)), (p1.y + (t*s1y))
    end
    return false
end

-- renders the initial light canvas,
-- adds a light and the corresponding casted shadows
function ShadowCaster:renderLightCanvas(lightCanvas, light)

    local stencil = function()
        if self.triangles then
            for _, v in pairs(self.triangles ) do
                love.graphics.polygon('fill', v)
            end
        end
    end
    love.graphics.setCanvas( lightCanvas )
        love.graphics.clear()
        love.graphics.setBlendMode('alpha')

        self:calculateShadows(collider, light)
        love.graphics.stencil(stencil, 'replace', 1)
        love.graphics.setStencilTest('equal', 1)
        local x, y = light:center()
        love.graphics.setColor(light.color or 255, 255, 255, 200)
        love.graphics.draw(light.image, x - (light.image:getWidth()*scale)/2, y - (light.image:getHeight()*scale)/2, 0, scale, scale)


        love.graphics.setStencilTest()
    love.graphics.setCanvas()

    return lightCanvas
end

function ShadowCaster:draw(light, color, mode)
    self:renderLightCanvas(self.lightCanvas, light)
    love.graphics.setColor(color or {255,255,255})
    love.graphics.setBlendMode(mode or 'screen', 'premultiplied')
    love.graphics.draw(self.lightCanvas)
end

-- *************** Light algorithm end

function love.draw(dt)

    love.graphics.setBackgroundColor(50, 20, 30, 255)

    for i in ipairs(obstacles) do
        love.graphics.setColor(50, 100, 200, 255)
        obstacles[i]:draw('fill')
    end

    -- lightrect.color = { love.math.random(230, 255), 100, 0 }
    lightrect2.color = { 0, 100, 250 }
    -- shadowcast1:renderLightCanvas(lightCanvas, lightrect)
    -- shadowcast2:renderLightCanvas(lightCanvas2, lightrect2)

    love.graphics.setColor(255,255,255)
    love.graphics.setBlendMode('screen', 'premultiplied')
    -- love.graphics.draw(lightCanvas)
    -- love.graphics.draw(lightCanvas2)

    shadowcast1:draw(lightrect, {255, 5, 0})
    shadowcast2:draw(lightrect2)

    for i=1, #manylights do
        manycasters[i]:draw(manylights[i])
    end

    love.graphics.setBlendMode('alpha')
    love.graphics.setColor(255, 255, 255)
    love.graphics.print( tostring(255))
    love.graphics.print("FPS: "..tostring(love.timer.getFPS()), 10, 10)
    -- print(inspect(love.graphics.getStats()))

end


function love.keypressed(key, scancode, isrepeat)
end

function love.keyreleased(key, scancode)
    if key == 'kpenter' then love.window.setMode( 1920, 1080, { vsync = true, resizable = true} ) end
    if key == 'escape' then
        love.event.quit()
    end
    if key == '+' or key == '-' then
        if key == '+' then
            scale = scale + 1
        else
            scale = scale - 1
            if scale < 1 then scale = 1 end
        end

        local image, color = lightrect.image, lightrect.color
        collider:remove(lightrect)
        lightrect = collider:rectangle(-1000, -1000, light:getWidth()*scale, light:getHeight()*scale)
        lightrect.image = image
        lightrect.color = color
        lightrect.light = true

        image, color = lightrect2.image, lightrect2.color
        collider:remove(lightrect2)
        lightrect2 = collider:rectangle(100, 100, light:getWidth()*scale, light:getHeight()*scale)
        lightrect2.image = image
        lightrect2.color = color
        lightrect2.light = true
    end

    if key == 'p' then
        ProFi:start()
        shadowcast1:draw(lightrect, {255, 255, 255})
        ProFi:stop()
        ProFi:writeReport('MyReport.txt')
    end

end

function love.mousepressed(x, y, button, istouch)
end

function love.mousereleased(x, y, button, istouch)
        for n in pairs(_G) do print(n) end
end

function love.mousemoved(x, y, dx, dy)
end

return
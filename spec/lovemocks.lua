-- mocks for LÖVE functions
local unpack = _G.unpack or table.unpack


local mt = {

  __tostring = function(self)
    local buffer = {}
    for i,v in ipairs(self) do
      buffer[i] = tostring(v)
    end
    return "{" .. table.concat(buffer, ",") .. "}"
  end

}

mt.__index = mt

_G.love = {
  graphics = {
    newCanvas = function(...)
      local arg = ... or {1280, 720, renderTo = function(target, source) return target end}
      return setmetatable(arg, mt)
    end,
    newImage = function(...)
      local arg = ... or {1280, 720, getWidth = function() return 1280 end, getHeight = function() return 720 end}
      return setmetatable(arg, mt)
    end,
    draw = function()
    end,
    getLastDrawq = function()
    end,
    setCanvas = function() end,
    setBlendMode = function() end,
    stencil = function() end,
    setStencilTest = function() end,
    setColor = function() end,
    clear = function() end,
    newImageData = function() end,
  }
}

_G.HC = {
  new = function(...)
            local arg = ... or {100}
            return setmetatable({arg}, mt)
        end
    }
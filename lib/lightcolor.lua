-- LightColor.lua
local LightColor = {}

-- Import any external globals we need
local love = love -- luacheck: ignore

local inspect = require 'lib.inspect' -- luacheck: ignore

-- no access to external globals
--_ENV = nil

-- create local variables

-- create local functions
local math_atan2 = math.atan2 or math.atan
local huge = math.huge
local floor = math.floor
local function round(n) return floor(n + 0.5) end -- luacheck: no unused

-- Vector Functions
local function vector()
    --[[ The content of this function is
    Copyright (c) 2010-2013 Matthias Richter

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    Except as contained in this notice, the name(s) of the above copyright holders
    shall not be used in advertising or otherwise to promote the sale, use or
    other dealings in this Software without prior written authorization.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
    ]]--
    local vec = {}
    vec.__index = vec

    local sqrt = math.sqrt

    local function new(x, y)
        return setmetatable({x = x or 0, y = y or 0}, vec)
    end
    local function isvec(v)
        return type(v) == 'table' and type(v.x) == 'number' and type(v.y) == 'number'
    end
    vec.__tostring = function(self) return "vec: (" .. tonumber(self.x) .. "," .. tonumber(self.y) .. ")" end
    vec.__mul = function(a,b)
        if type(a) == "number" then
            return new(a*b.x, a*b.y)
        elseif type(b) == "number" then
            return new(b*a.x, b*a.y)
        else
            assert(isvec(a) and isvec(b), "Mul: wrong argument types (<vector> or <number> expected)")
            return a.x*b.x + a.y*b.y
        end
    end

    vec.len2 = function(self) return self.x*self.x + self.y*self.y end
    vec.len = function(self) return sqrt(self.x*self.x + self.y*self.y) end
    vec.normalizeInplace = function(self)
                            local l = self:len()
                            if l>0 then self.x, self.y = self.x / l, self.y / l end
                            return self
                        end
    vec.perpendicular = function(self) return new(-self.y, self.x) end
    vec.angleTo = function(self, other)
                        if other then
                            return math_atan2(self.y, self.x) - math_atan2(other.y, other.x) end
                        return math_atan2(self.y, self.x)
                    end


    return setmetatable({new = new}, {__call = function(_, ...) return new(...) end})
end
local vector = vector() -- luacheck: ignore

-- Lian Barsky Algorithm, will be called down the line to snip edges
local _clipEdge = function(clipLeft, clipDown, clipRight, clipUp, edgestartvert, edgestopvert)
    -- preliminary setup
    local t0, t1 = 0.0, 1.0
    local P0, P1 = edgestartvert, edgestopvert
    local dx, dy = P1.x - P0.x, P1.y - P0.y

    -- Liang-Basrky line clipping algorithm testing the edges
    local clipAlgo = function(P, Q, R)
        --edge check itself
        -- if all the line is out of box (i.e. parrallel to  edge) we can stop here
        if P == 0 and Q < 0 then
            return false
        -- observe the edge (in case of line going away from edge)
        elseif P < 0 then
            if R > t1 then
                return false
            -- line is actually longer than edge -> clip it!
            elseif R > t0 then
                t0 = R
            end
        -- case: line coming towards edge
        elseif P > 0 then
            if R < t0 then
                return false
            elseif R < t1 then
                t1 = R
            end
        end
    end

    --left edge check
    local P = -dx
    local Q = -1*(clipLeft-P0.x)
    local R = Q/P
    clipAlgo(P, Q, R)
    --right edge check
    P = dx
    Q = clipRight-P0.x
    R = Q/P
    clipAlgo(P, Q, R)
    --down edge check
    P = -dy
    Q = -(clipDown-P0.y)
    R = Q/P
    clipAlgo(P, Q, R)
    --up edge check
    P = dy
    Q = clipUp-P0.y
    R = Q/P
    clipAlgo(P, Q, R)

    -- finally calculate new start and end point, based on t0, t1
    if t0 == 0 and t1 == 1 then
        -- nothing to do, line was already completely inside
        return P0, P1
    else
        -- return new start and end point
        local newP0 = { x = P0.x + (t0*dx), y = P0.y + t0*dy }
        local newP1 = { x = P0.x + t1*dx, y = P0.y + t1*dy }

        return newP0, newP1
    end
end

-- Simple Angle Calculation between two points
local angle = function(x1, y1, x2, y2) return math_atan2(y2-y1, x2-x1) end

-- Intersection function between two line segments
-- http://stackoverflow.com/a/1968345
local _findIntersection = function(p1, p2, p3, p4)
    local s1x = p2.x - p1.x
    local s1y = p2.y - p1.y
    local s2x = p4.x - p3.x
    local s2y = p4.y - p3.y

    local s = (-s1y * (p1.x - p3.x) + s1x * (p1.y - p3.y)) / (-s2x * s1y + s1x * s2y)
    local t = ( s2x * (p1.y - p3.y) - s2y * (p1.x - p3.x)) / (-s2x * s1y + s1x * s2y)

    if (s >= 0 and s <=1 and t >= 0 ) then
        --collision detected
        return (p1.x+(t*s1x)), (p1.y + (t*s1y))
    end
    return false
end

function LightColor.new(self)
    local newInstance = nil
    if self then
        newInstance = {}
        setmetatable(newInstance, self)
        self.__index = self
    end
    return newInstance or LightColor
end

-- TODO add differently shaped lights?
-- TODO add circular lights etc; point, spot (directional)
function LightColor:light(hc, x, y, r, color, image)
    return self:addPointLight(hc, x, y, r, color, image)
end

function LightColor:addPointLight(hc, x, y, w, h) -- luacheck: no unused
    local l = hc:rectangle(x, y, w, h)
    l.render = function(light, canvas, gatherCollisions, drawFunction)
        return self:render(canvas, gatherCollisions, drawFunction, light)
    end
    return l
end

function LightColor:getStencil(collisionFunction, l)
    local triangles = self:_calculateShadows(collisionFunction, l)
    return function()
        if  triangles ~= false then
            for _, t in ipairs(triangles) do
                love.graphics.polygon('fill', t)
            end
        end
    end
end

function LightColor:render(canvas, gatherCollisions, drawFunction, ...)
    for i = 1, select('#', ...) do
        local l = select(i, ...)

        canvas:renderTo( function()
            love.graphics.stencil(self:getStencil(gatherCollisions, l), 'replace', 1)
            drawFunction(l)
        end)
    end

    return canvas
end

function LightColor:_calculateShadows(gatherCollisions, light, centerX, centerY) -- luacheck: no unused
    if centerX == nil or centerY == nil then
        centerX, centerY = light:center()
    end

    return self:createPolygonFromRaycast(self:getClosestEdges(gatherCollisions, light, centerX, centerY), centerX, centerY)
end

-------- LIGHT ALGORITHM
function LightColor:getClosestEdges(gatherCollisions, boundaryShape, centerX, centerY) -- luacheck: no unused
    local boundary = boundaryShape
    local boundaryX, boundaryY = centerX, centerY

    -- gather all objects colliding with boundary
    -- remove other lights colliding with boundary
    local collisions = gatherCollisions(boundary)

    -- find the closest edges, relative to boundaryX & boundaryY
    -- TODO: only works for rectangular/polygon hc shapes (ones with ._polygon.vertices)
    local closestEdges = {}
    for shape, _ in pairs(collisions) do
        local verts = shape._polygon.vertices
        local inside = true
        for k, v in ipairs(verts) do
            local startvert = v
            local endvert = verts[k+1] or verts[1]

            local centervec = vector(startvert.x - boundaryX, startvert.y - boundaryY)
            local edgevec = vector(endvert.x - startvert.x, endvert.y - startvert.y)

            -- find actual closest vector by multiplying normal of edgevector with boundary -> start/end vertice vec (dot product)
            -- do this by finding normals and keeping those pointing towards boundary
            -- positive normal > 'outside' vector, keep
            -- negative normal > 'inside' vector, discard
            -- used > 1 instead > 0 to give a little leeway, hopefully minimizing artifacts
            local edgenormal = edgevec:perpendicular():normalizeInplace()
            if edgenormal * centervec > 0 then
                closestEdges[#closestEdges+1] = { a = startvert, b = endvert}
                inside = false
            end
        end

        -- if all edges are inside -> boundary origin is inside an object
        if inside == true then return false end
    end

    -- TODO: implement edge check function for polygons, not needed yet
    -- check if edge itself is within boundary bounds, otherwise discard it
    -- mostly useful for really big polygonal shapes

    -- clip the edge to the borders of boundaryShape
    -- using Lian Barsky Algorithm -> only works for rectangular boundaryShape!
    local _, up = boundary:support(0, 1)
    local _, down = boundary:support(0, -1)
    local left = boundary:support(-1, 0)
    local right = boundary:support(1, 0)

    for _, v in ipairs(closestEdges) do
        local newA, newB = _clipEdge(left, down, right, up, v.a, v.b)
        if newA ~= false then
            v.a = newA
            v.b = newB
        end
        v.a.x = floor(v.a.x)
        v.a.y = floor(v.a.y)
        v.b.x = floor(v.b.x)
        v.b.y = floor(v.b.y)
    end

    -- finally add boundary edges themselves, have to be added last!
    local emverts = boundary._polygon.vertices
    for k, v in ipairs(emverts) do
        closestEdges[#closestEdges+1] = { a = v, b = emverts[k+1] or emverts[1]}
    end

    -- TODO: inject filterfunction into method?
    -- maybe hc has implemented filter function already?
    return closestEdges
end

-- create polygon encompassing given edges
-- parameters
-- edgeList: list of edges in the form of { a = { x =  y = startvert}, b = { x = y = endvert}}
-- centerX, centerY: coorinates of centerpoint for polygon & raycast
-- returns list of triangles that can build the resulting polygon (i.e. coordinates of the polygon triangulated)
function LightColor:createPolygonFromRaycast(edgeList, centerX, centerY) -- luacheck: no unused
    if edgeList == false then return false end
    return self:createTriangulatedPolygon(
                self:findEnclosingIntersections(
                    self:getSortedRays(edgeList, centerX, centerY)))
end

function LightColor:getSortedRays(edgeList, centerX, centerY) -- luacheck: no unused
    -- gather fan of rays to every edge vertex
    -- also remember their corresponding edge
    local rays = {}
    for k, v in ipairs(edgeList) do
        local uniqueRayToA = true
        local uniqueRayToB = true
        for _, ray in ipairs(rays) do
            local eVecA = vector(v.a.x-centerX, v.a.y-centerY)
            local eVecB = vector(v.b.x-centerX, v.b.y-centerY)
            local rVec = vector(ray.x-centerX, ray.y-centerY)
            if eVecA:angleTo(rVec) == 0 then
                uniqueRayToA = false
                if eVecA:len2() == rVec:len2() then ray.edgevert = nil end
            end
            if eVecB:angleTo(rVec) == 0 then
                uniqueRayToB = false
                if eVecB:len2() == rVec:len2() then ray.edgevert = nil end
            end
        end
        if uniqueRayToA == true then
            rays[#rays+1] = { x = v.a.x, y = v.a.y, id = k, edgevert = 1 }
        end
        if uniqueRayToB == true then
            rays[#rays+1] = { x = v.b.x, y = v.b.y, id = k, edgevert = 2 }
        end
    end

    -- sort rays by angle
    table.sort(rays, function(a, b) return angle(centerX, centerY, a.x, a.y) < angle(centerX, centerY, b.x, b.y) end )
    return rays, centerX, centerY, edgeList
end

function LightColor:findEnclosingIntersections(rayEndPoints, centerX, centerY, edges) -- luacheck: no unused
    local intersections = {}

    for _, ray in ipairs(rayEndPoints) do
        local p_self = {}
        local p_other = {}
        local shortest = huge

        for idE, edge in ipairs(edges) do
            local resultX, resultY = _findIntersection({x = centerX, y = centerY}, ray, edge.a, edge.b)

            -- found intersection
            if resultX ~= false then
                local length = vector(resultX - centerX, resultY - centerY):len2()

                if ray.id == idE then
                    -- ray vert belongs to the edge it intersects with
                    -- add to p_self, so we can later check for something closer
                    p_self = { x = resultX, y = resultY }
                    p_self.edgevert = ray.edgevert
                elseif length < shortest then
                    -- ray vert does not belong to the edge it intersects with
                    -- and the length is closer than the last found intersection
                    -- add it as p_other, it is definitely the closest one (so far)
                    p_other = { x = resultX, y = resultY }
                    shortest = length
                end
            end
        end

        local p_selfLen = vector((p_self.x or huge) - centerX, (p_self.y or huge) - centerY):len2()
        local p_otherLen = vector((p_other.x or huge) - centerX, (p_other.y or huge) - centerY):len2()

        -- vert is at right side ('end') of corresponing edge -> add it before others
        if p_self.edgevert == 1 and p_selfLen < p_otherLen then
            p_self.edgevert = nil
            intersections[#intersections+1] = p_self
        end

        --only add other if it is NOT the exact same location as another vert already
        if p_other.x ~= nil then
            intersections[#intersections+1] = p_other
        end

        -- vert is at left side ('start') of edge -> add it after the p_other vert
        if p_self.edgevert == 2 and p_selfLen < p_otherLen then
            p_self.edgevert = nil
            intersections[#intersections+1] = p_self
        end
    end
    return intersections, centerX, centerY
end

function LightColor:createTriangulatedPolygon(intersections, centerX, centerY) -- luacheck: no unused
    local verts = {}
    for k, v in ipairs(intersections) do
        local nextpos = intersections[k+1] or intersections[1]
        verts[#verts+1] = { floor(centerX),floor(centerY), floor(v.x), floor(v.y), floor(nextpos.x), floor(nextpos.y) }
    end
    return verts
end

return LightColor
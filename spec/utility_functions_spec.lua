-- package.path = package.path .. ';./?.lua;./?/?.lua;./?/init.lua'

-- debug import
local inspect = require 'lib.inspect'


-- helper mock & library imports
require 'spec.lovemocks'
local HC = require 'lib.HC'


-- module to be tested
local LC = require("lib.lightcolor")

describe("LightColor", function()
    describe(":new", function ()

        local lc, s

        after_each(function()
            lc = nil
        end)

        it('returns a new instance of itself', function()
            lc = LC.new(self)
            lc2 = LC:new()
            LCSYS = LC.new()

            assert.same(lc, lc2)
            assert.is_not_equal(lc2, lc)
            assert.same(LCSYS, lc2)
            assert.is_not_equal(LCSYS, lc2)
        end)

        teardown(function()
            s = nil
            lc = nil
        end)
    end)

    describe(":light", function()
        it("serves as an alias for :addPointLight", function()
            lc = LC:new()
            hc = HC.new()
            spy.on(lc, 'addPointLight')

            l = lc:light(hc, 100, 100, 50, 50)
            assert.spy(lc.addPointLight).was_called(1)
        end)
    end)

    describe(":addPointLight", function()
        local lc, l, s

        setup(function()
            hc = HC.new()
        end)

        after_each(function ()
            lc = nil
            l = nil
            s = nil
        end)

        it("creates a HC rectangle with the parameters as vertices, changed for a rectangle", function()
            lc = LC:new()
            s = spy.on(hc, 'rectangle')
            l = lc:addPointLight(hc, 50, 50, 100, 100)

            assert.spy(hc.rectangle).was_called_with(match.is_table(), 50, 50, 100, 100)
        end)

        it("returns the light that was just created", function()
            lc = LC:new()
            s = spy.on(lc, 'addPointLight')
            l = lc:addPointLight(hc, 100, 100, 50, 50)

            assert.spy(lc.addPointLight).returned_with(l)
        end)
    end)
end)


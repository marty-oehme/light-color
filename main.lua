--all the available libs listed
local HC = require "lib.HC"
inspect = require 'lib.inspect'
vector = require 'lib.hump.vector'
lume = require 'lib.lume'
ProFi = require 'lib.ProFi'

local LightColor = require "lib.lightcolor"

local objects = {}
local lc, hc, testlight

local canvas = love.graphics.newCanvas()

local blendmode = -1
local debugDisplayMode = 0

--- DEBUG FUNCTIONS END ---

function love.load(arg)
    lc = LightColor:new()
    hc = HC.new(250)
    math.randomseed(os.time())
    math.random()
    math.random()
    math.random()
    objects[#objects+1] = hc:rectangle(100, 10, 30, 30)
    objects[#objects+1] = hc:rectangle(60, 10, 30, 30)
    objects[#objects+1] = hc:polygon(0, 50, 10, 25, 30, 20, 50, 30, 55, 55)

    local poly = function(n, r, out)
        out = out or {}
        local i = 1
        for j = 0, n do
            local a = j/n*math.pi*2
            out[i] = math.cos(a)*r
            out[i + 1] = math.sin(a)*r
            i = i + 2
        end
        return out
    end


    for i = 1, 35 do
        local ranx = math.random(50, love.graphics.getWidth())
        local rany = math.random(50, love.graphics.getHeight())
        local poly = hc:polygon(unpack(poly(math.random(4, 6), math.random(15, 75))))
        repeat
            ranx = math.random(50, love.graphics.getWidth())
            rany = math.random(50, love.graphics.getHeight())
            poly:moveTo(ranx, rany)
            local col = 0
            for i,_ in pairs(hc:collisions(poly)) do
                col = 1
                break
            end
        until col == 0
        objects[#objects+1] = poly
    end

    --naive implementation of softlight
    testlight = lc:light(hc, 50, 50, 256, 256)
    l2 = lc:light(hc, 50, 50, 256, 256)
    l3 = lc:light(hc, 50, 50, 256, 256)
    l4 = lc:light(hc, 50, 50, 256, 256)
    l5 = lc:light(hc, 50, 50, 256, 256)
    l6 = lc:light(hc, 50, 50, 256, 256)
    l7 = lc:light(hc, 50, 50, 256, 256)
    l8 = lc:light(hc, 50, 50, 256, 256)
    l9 = lc:light(hc, 50, 50, 256, 256)
    testlight.light = {200, 0, 0, 33}
    testlight.lightImage = love.graphics.newImage('light_softer.png')
    l2.light = {200, 0, 0, 33}
    l2.lightImage = love.graphics.newImage('light_softer.png')
    l3.light = {200, 0, 0, 33}
    l3.lightImage = love.graphics.newImage('light_softer.png')
    l4.light = {200, 0, 0, 33}
    l4.lightImage = love.graphics.newImage('light_softer.png')
    l5.light = {200, 0, 0, 33}
    l5.lightImage = love.graphics.newImage('light_softer.png')
    l6.light = {200, 0, 0, 33}
    l6.lightImage = love.graphics.newImage('light_softer.png')
    l7.light = {200, 0, 0, 33}
    l7.lightImage = love.graphics.newImage('light_softer.png')
    l8.light = {200, 0, 0, 33}
    l8.lightImage = love.graphics.newImage('light_softer.png')
    l9.light = {200, 0, 0, 33}
    l9.lightImage = love.graphics.newImage('light_softer.png')

    lights = {}
    for i = 1, 36 do
        lights[i] = lc:light(hc, math.random(love.graphics.getWidth()), math.random(love.graphics.getHeight()), 256, 256)
        lights[i].mov = { x = math.random(-2, 2), y = math.random(-2, 2)}
        lights[i].light = {math.random(50, 150), math.random(50, 150), math.random(50, 150), math.random(50, 100)}
        lights[i].lightImage = love.graphics.newImage('light_softer.png')
    end

end



function love.update(dt)
    local posX, posY = love.mouse.getPosition()
    testlight:moveTo(posX, posY)
    l2:moveTo(posX+1, posY)
    l3:moveTo(posX-1, posY)
    l4:moveTo(posX, posY+1)
    l5:moveTo(posX, posY-1)
    l6:moveTo(posX+2, posY)
    l7:moveTo(posX-2, posY)
    l8:moveTo(posX, posY+2)
    l9:moveTo(posX, posY-2)

    for k, v in ipairs(lights) do
        local x, y = v:center()
        --v:move(v.mov.x, v.mov.y)
        if x > love.graphics.getWidth() then v:moveTo(0, y) end
        if y > love.graphics.getHeight() then v:moveTo(x, 0) end
    end

end

function love.draw(dt)
    love.graphics.setBackgroundColor(10, 5, 5, 100)

    -- draw obstacles and fluff
    for i in ipairs(objects) do
        love.graphics.setColor(50, 100, 200, 255)
        objects[i]:draw('fill')
    end

    local function drawLights(light)
        local x, y = light:center()
        love.graphics.setStencilTest('greater', 0)
        love.graphics.setColor(light.light or 255, 0, 0, 50)
        love.graphics.draw(light.lightImage,x - light.lightImage:getWidth() * 0.5, y - light.lightImage:getHeight() * 0.5)
        love.graphics.setColor(255, 255, 255, 255)
        love.graphics.setStencilTest()
    end

    local function getObstacles(obj)
        local collisions = hc:collisions(obj)
        for coll, _ in pairs(collisions) do
            if coll.light then
                collisions[coll] = nil
            end
        end
        return collisions
    end
    canvas:renderTo(function () love.graphics.clear() end)


    -- draw the debug stuff
    if debugDisplayMode == 0 then
        canvas = lc:render(canvas, getObstacles, drawLights, l2, l3, l4, l5, l6, l7, l8, l9, unpack(lights))
        testlight:render(canvas, getObstacles, drawLights)
        if blendmode == -1 then
            love.graphics.setBlendMode('screen', 'premultiplied')
        else
            love.graphics.setBlendMode('alpha')
        end
        love.graphics.draw(canvas)

        love.graphics.setBlendMode('alpha')
        love.graphics.print({{0, 255, 0}, "Shadows, Blendmode: ", tostring((blendmode==1) and "alpha" or "premultiplied")}, 10, 10)
    elseif debugDisplayMode == 1 then
        -- visualize the borders affected by light
        local edges = lc:getClosestEdges(getObstacles, testlight, testlight:center())
        if edges then
            for i in ipairs(edges) do
                love.graphics.setLineJoin('none')
                love.graphics.setColor(255, 0, 0, 255)
                love.graphics.line(edges[i].a.x, edges[i].a.y, edges[i].b.x, edges[i].b.y)
            end
        end
        love.graphics.print({{0, 255, 0}, "Object Edges"}, 10, 10)
    elseif debugDisplayMode == 2 then
        -- visualize the rays cast by algorithm
        local edges = lc:getClosestEdges(getObstacles, testlight, testlight:center())
        if edges ~= false then
            edges = lc:getSortedRays(edges, testlight:center())
        else
            edges = {}
        end
        local cenX, cenY = testlight:center()
        for i in ipairs(edges) do
            love.graphics.setLineJoin('none')
            love.graphics.setColor(255, 0, 0, 255)
            love.graphics.line(cenX, cenY, edges[i].x, edges[i].y)
        end
        love.graphics.print({{0, 255, 0}, "Rays"}, 10, 10)
    elseif debugDisplayMode == 3 then
        -- visualize the individual polygons created by algorithm
        local triangles = lc:_calculateShadows(getObstacles, testlight)
        if triangles then
            for i, t in ipairs(triangles) do
                love.graphics.setColor(255, 0, 0, 255)
                love.graphics.polygon('line', t)
            end
        end
        love.graphics.print({{0, 255, 0}, "Polygon Edges"}, 10, 10)
    end
    love.graphics.print("Current FPS: "..tostring(love.timer.getFPS( )), love.graphics.getWidth()-100, 10)
end


function love.keypressed(key, scancode, isrepeat)
end


local function profilerFunction()
    local function drawLights(light)
        local x, y = light:center()
        love.graphics.setStencilTest('greater', 0)
        love.graphics.setColor(light.light)
        love.graphics.draw(light.lightImage,x - light.lightImage:getWidth() * 0.5, y - light.lightImage:getHeight() * 0.5)
        love.graphics.setColor(255, 255, 255, 255)
        love.graphics.setStencilTest()
    end

    local function getObstacles(obj)
        local collisions = hc:collisions(obj)
        for coll, _ in pairs(collisions) do
            if coll.light then
                collisions[coll] = nil
            end
        end
        return collisions
    end
    canvas:renderTo(function () love.graphics.clear() end)

    ProFi:start()
        canvas = lc:render(canvas, getObstacles, drawLights, testlight, l2, l3, l4, l5, l6, l7, l8, l9, unpack(lights))
        love.graphics.draw(canvas)
    ProFi:stop()
    ProFi:writeReport()
    ProFi:reset()
end

function love.keyreleased(key, scancode)
    if key == 'kpenter' then
        love.window.setMode( 1920, 1080, { vsync = true, resizable = true} )
    end
    if key == 'escape' then
        love.event.quit()
    end

    if key == 'm' then debugDisplayMode = debugDisplayMode + 1 end
    if debugDisplayMode > 4 then debugDisplayMode = 0 end
    if key == 'b' then blendmode = blendmode * -1 end
    if key == 'p' then
        -- ProFi:start()
        -- shadowcast1:draw(lightrect, {255, 255, 255})
        -- ProFi:stop()
        -- ProFi:writeReport('MyReport.txt')
        profilerFunction()
    end

end


function love.mousepressed(x, y, button, istouch)
end

function love.mousereleased(x, y, button, istouch)
        for n in pairs(_G) do print(n) end
end

function love.mousemoved(x, y, dx, dy)
end

return
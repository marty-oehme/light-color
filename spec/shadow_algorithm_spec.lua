-- package.path = package.path .. ';./?.lua;./?/?.lua;./?/init.lua'

-- debug import
local inspect = require 'lib.inspect'


-- helper mock & library imports
require 'spec.lovemocks'
local HC = require 'lib.HC'


-- module to be tested
local LC = require("lib.lightcolor")

describe("LightColor", function()
    describe(":_calculateShadows", function()
        before_each(function()
            hc = HC.new()
            collisions = function(obj) return hc:collisions(obj) end
        end)

        it("calls createPolygonFromRaycast and getClosestEdges", function()
            lc = LC:new()
            s = spy.on(lc, 'createPolygonFromRaycast')
            s2 = spy.on(lc, 'getClosestEdges')

            lc:_calculateShadows(collisions, lc:light(hc, 5, 5, 5, 5), {})

            assert.spy(lc.createPolygonFromRaycast).was_called(1)
            assert.spy(lc.getClosestEdges).was_called(1)
        end)

        it("can optionally take centerX and centerY coordinates which it passes on", function()
            lc = LC:new()
            s = spy.on(lc, 'createPolygonFromRaycast')
            s2 = spy.on(lc, "getClosestEdges")

            light = lc:light(hc, 60, 60, 20, 20)
            lc:_calculateShadows(collisions, light, 50, 50)

            assert.spy(lc.createPolygonFromRaycast).called_with(lc, match.is_table(), 50, 50)
            assert.spy(lc.getClosestEdges).called_with(lc, match._, light, 50, 50)

        end)
    end)

    describe(":getClosestEdges", function()
        before_each(function ( )
            hc = HC.new()
            lc = LC:new()
            collisions = function(obj) return hc:collisions(obj) end
        end)

        it("returns closest edges of all objects in input hc system and emitter itself, clipped to dimensions of emitter", function()

            light = lc:light(hc, 0, 0, 100, 100)
            hc:rectangle( 10, 10, 20, 20)
            hc:rectangle( 135, 135, 30, 30)
            hc:rectangle( 90, 70, 20, 20)

            result = lc:getClosestEdges(collisions, light, light:center())
            goal = {
                { a = { x = 30, y = 10 },
                   b = { x = 30, y = 30 } },
                { a = { x = 30, y = 30 },
                   b = { x = 10, y = 30 } },
                { a = { x = 90, y = 90 },
                   b = { x = 90, y = 70 } },
                { a = { x = 90, y = 70 },
                   b = { x = 100, y = 70 } },
                { a = { x = 0, y = 0 },
                   b = { x = 100, y = 0 } },
                { a = { x = 100, y = 0 },
                   b = { x = 100, y = 100 } },
                { a = { x = 100, y = 100 },
                   b = { x = 0, y = 100 } },
                { a = { x = 0, y = 100 },
                   b = { x = 0, y = 0 } },
            }

            local sortFunc = function(a, b)
                if a.a.x ~= b.a.x then return a.a.x < b.a.x end
                return a.a.y < b.a.y
            end
            table.sort(goal, sortFunc)
            table.sort(result, sortFunc)
            assert.same(goal, result)
        end)

        it("does not add edges that are directly perpendicular to origin #bugfix #edgebug", function()

            hc:polygon(100, 21, 150, 100, 150, 150, 60, 60)
            local testLight = lc:light(hc, 0, 0, 100, 100)

            -- if it would add it there would be an edge from 60,60 -> 150, 150 in here
            local goal = { { a = { x = 60, y = 60 },
                           b = { x = 100, y = 21 } },
                        { a = { x = 0, y = 100 },
                           b = { x = 0, y = 0 } },
                        { a = { x = 0, y = 0 },
                           b = { x = 100, y = 0 } },
                        { a = { x = 100, y = 0 },
                           b = { x = 100, y = 100 } },
                        { a = { x = 100, y = 100 },
                           b = { x = 0, y = 100 } }
                       }
            result = lc:getClosestEdges(collisions, testLight, testLight:center())

            assert.same(goal, result)
        end)

        it("outputs false if emitter origin is inside obstacle", function()

            light = lc:light(hc, 0, 0, 100, 100)
            hc:rectangle( 40, 40, 20, 20)

            assert.is_false(lc:getClosestEdges(collisions, light, light:center()))
        end)
    end)

    describe(":getSortedRays", function()
        -- casts rays from given point to start and end point of line segments
        --returns sorted
        before_each(function()
            hc = HC.new()
            lc = LC:new()
        end)

        it("returns rays cast from given point to all edge endpoints", function()
            local edges =  {
                        {   a = { x = 10, y = 35 },
                            b = { x = 35, y = 35} },
                        {   a = { x = 35, y = 35},
                            b = { x = 35, y = 0} },
                        {   a = { x = 48, y = 5},
                            b = { x = 48, y = 32} },
                        {   a = { x = 48, y = 32},
                            b = { x = 73, y = 32} },
                        {   a = { x = 20, y = 40},
                            b = { x = 40, y = 55} },
                        {   a = { x = 40, y = 55},
                            b = { x = 70, y = 52} },
                        {   a = { x = 70, y = 52},
                            b = { x = 84, y = 44} },
                        }


            result = lc:getSortedRays(edges, 42, 44)
            local goal = {
                        { x = 20, y = 40, id = 5 , edgevert = 1},
                        { x = 10, y = 35, id = 1 , edgevert = 1},
                        { x = 35, y = 35, id = 1 },
                        { x = 35, y = 0 , id = 2 , edgevert = 2},
                        { x = 48, y = 5 , id = 3 , edgevert = 1},
                        { x = 48, y = 32, id = 3 },
                        { x = 73, y = 32, id = 4 , edgevert = 2},
                        { x = 84, y = 44, id = 7 , edgevert = 2},
                        { x = 70, y = 52, id = 6 },
                        { x = 40, y = 55, id = 5 }
            }
            assert.same(goal, result)
        end)

        it("will only add the shortest ray if multiple have the same angle #bugfix #edgebug", function()

            hc:polygon(100, 21, 150, 100, 150, 150, 60, 60)
            local testLight = lc:light(hc, 50, 50, 50, 50)

            local edges = { { a = { x = 60, y = 60 },
                           b = { x = 100, y = 21 } },
                        { a = { x = 0, y = 100 },
                           b = { x = 0, y = 0 } },
                        { a = { x = 0, y = 0 },
                           b = { x = 100, y = 0 } },
                        { a = { x = 100, y = 0 },
                           b = { x = 100, y = 100 } },
                        { a = { x = 100, y = 100 },
                           b = { x = 0, y = 100 } }
                       }

            -- does NOT contain { x = 100, y = 100, id = 4} which it would if all endpoints were added
            local goal = {
                            { x = 0, y = 0, id = 2 },
                            { x = 100, y = 0, id = 3},
                            { x = 100, y = 21, id = 1, edgevert = 2},
                            { x = 60, y = 60, id = 1, edgevert =  1},
                            { x = 0, y = 100, id = 2},
                            }
            result = lc:getSortedRays(edges, 50, 50)

            assert.same(goal, result)
        end)
    end)

    describe(":findEnclosingIntersections", function()
        --finds ray and edge intersections - more generally btw 2 line segments?
        before_each(function()
            hc = HC.new()
            lc = LC:new()
        end)

        it("iterates through all rays and finds closest intersection with an edge", function()
            local edges = {{   a = { x = 10, y = 35 },
                                b = { x = 35, y = 35} },
                            {   a = { x = 35, y = 35},
                                b = { x = 35, y = 0} },
                            {   a = { x = 48, y = 5},
                                b = { x = 48, y = 32} },}
            local rayEnds = { { edgevert = 1, id = 1, x = 10, y = 35 }, { id = 1, x = 35, y = 35 }, { edgevert = 2, id = 2, x = 35, y = 0 }, { edgevert = 1, id = 3, x = 48, y = 5 }, { edgevert = 2, id = 3, x = 48, y = 32 } }
            local rayStart = {x = 42, y = 44}
            result = lc:findEnclosingIntersections(rayEnds, rayStart.x, rayStart.y, edges)

            local goal = {  { x = 10, y = 35},
                            { x = 35, y = 35},
                            { x = 35, y = 0},
                            { x = 48, y = 5},
                            { x = 48, y = 32}, }
            assert.same(goal, result)
        end)

        it("works for edges going straight out from the center #bugfix #edgebug", function()
            local edges = { { a = { x = 60, y = 60 },
                           b = { x = 100, y = 21 } },
                        { a = { x = 0, y = 100 },
                           b = { x = 0, y = 0 } },
                        { a = { x = 0, y = 0 },
                           b = { x = 100, y = 0 } },
                        { a = { x = 100, y = 0 },
                           b = { x = 100, y = 100 } },
                        { a = { x = 100, y = 100 },
                           b = { x = 0, y = 100 } }
                       }
            local rayEnds = {
                            { x = 0, y = 0, id = 2 },
                            { x = 100, y = 0, id = 3},
                            { x = 100, y = 21, id = 1, edgevert = 2},
                            { x = 60, y = 60, id = 1, edgevert =  1},
                            { x = 100, y = 100, id = 4},
                            { x = 0, y = 100, id = 2},
                            }
            local rayStart = { x = 50, y = 50 }

            result = lc:findEnclosingIntersections(rayEnds, rayStart.x, rayStart.y, edges)
            local goal = {  { x = 0, y = 0} ,
                            { x = 100, y = 0} ,
                            { x = 100, y = 21} ,
                            { x = 60, y = 60} ,
                            { x = 100, y = 100} ,
                            { x = 60, y = 60} ,
                            { x = 0, y = 100}
                        }

            assert.same(goal, result)
        end)
    end)

    describe(":createPolygonFromRaycast", function()
        it("returns the vertex coordinates for a polygon, rounded down to exact pixel values", function()
            lc = LC:new()
            local edges =   {{ a = { x = 1, y = 6 }, b = { x = 4, y = 8 }},
                            { a = { x = 8, y = 2 }, b = { x = 4, y = 2 }},
                            { a = { x = 0, y = 0 }, b = { x = 10, y = 0 }},
                            { a = { x = 10, y = 0 }, b = { x = 10, y = 10 }},
                            { a = { x = 10, y = 10 }, b = { x = 0, y = 10 }},
                            { a = { x = 0, y = 10 }, b = { x = 0, y = 0 }},
                                }
            local goal = { { 5, 5, 0, 0, 3, 0 }, -- this would be 3.33(..) if not rounded down
                         { 5, 5, 3, 0, 4, 2 }, -- this would be 3.33(..) if not rounded down
                         { 5, 5, 4, 2, 8, 2 },
                         { 5, 5, 8, 2, 10, 0 },
                         { 5, 5, 10, 0, 10, 10 },
                         { 5, 5, 10, 10, 3, 10 }, -- this would be 3.33(..) if not rounded down
                         { 5, 5, 3, 10, 4, 8 }, -- this would be 3.33(..) if not rounded down
                         { 5, 5, 4, 8, 2, 7 },
                         { 5, 5, 2, 7, 1, 6 },
                         { 5, 5, 1, 6, 0, 6 },
                         { 5, 5, 0, 6, 0, 0 } }
            assert.same(goal, lc:createPolygonFromRaycast(edges, 5, 5))
        end)
    end)
end)